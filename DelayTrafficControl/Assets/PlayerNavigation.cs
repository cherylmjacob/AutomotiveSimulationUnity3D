﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerNavigation : MonoBehaviour
{


    public Path pathtofollow;

    public HaloTrafficLight trafficLightState;
    public int i = 0;

    public Camera cam;
    public NavMeshAgent navagent;
    public NavMeshPath navpath;

    float x1, x2, z1, z2 ,x3,x4,z3,z4,x5,x6,z5,z6;

    GameObject dest;
    // Use this for initialization
    void Start()
    {
        cam = Camera.main;
        navagent = GetComponent<NavMeshAgent>();
        
    }

  int noOfHits;
    // Update is called once per frame
    void Update()
    {
        /*  if((tag1.position.x1>tag2.position.x2)) //x1=pathtofollow.path_objs[1].speed;
                                                  //x2=pathtofollow.path_objs[2].speed; 
           {
               public RaycastHit;
               navagent.SetDestination(pathtofollow.path_objs[1].Raycasthitposition);
           }
       */

        RaycastHit hit;

        Ray backwardRay = new Ray(transform.position, Vector3.back);

        if (Physics.Raycast(backwardRay, out hit, 10))
        {
            if (hit.collider.tag == "CarCollider")
            {
                Debug.DrawRay(transform.position, Vector3.back * 10);
                IncrementCount();
            }
        }
        if (i == 0)
        {
            navagent.SetDestination(pathtofollow.path_objs[0].position);
           
        }
        Debug.Log("Path pos" + pathtofollow.path_objs[0].position);
        Debug.Log("obj pos" + transform.position);
        x1 = transform.position.x;
        x2 = pathtofollow.path_objs[0].position.x;
        z1 = transform.position.z;
        z2 = pathtofollow.path_objs[0].position.z;



        if (x1 == x2 && z1 == z2)
        {
            i = 1;
        }

            if (i == 1)
            {
                navagent.SetDestination(pathtofollow.path_objs[1].position);
            }
            x3 = transform.position.x;
            x4 = pathtofollow.path_objs[1].position.x;
            z3 = transform.position.z;
            z4 = pathtofollow.path_objs[1].position.z;

            //Debug.Log("Path posx" + pathtofollow.path_objs[1].position.x);
            //Debug.Log("obj posx" + transform.position.x);
            //Debug.Log("Path posz" + pathtofollow.path_objs[1].position.z);
            //Debug.Log("obj posz" + transform.position.z);
            if (x3 == x4 && z3 == z4)
            {
                i = 2;
            }


        if (i == 2)
        {
            navagent.SetDestination(pathtofollow.path_objs[2].position);
        }
        x5 = transform.position.x;
        x6 = pathtofollow.path_objs[2].position.x;
        z5 = transform.position.z;
        z6 = pathtofollow.path_objs[2].position.z;
        if (x5 == x6 && z5 == z6)
        {
            i = 3;
        }

        if (i >=3)
            {
            navagent.speed = 0.0001f;
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_WEBPLAYER
                         Application.OpenURL(webplayerQuitURL);
#else
                         Application.Quit();
#endif
        }

    }

    void IncrementCount()
    {
        noOfHits++;
    }
    
    void OnTriggerStay(Collider col)
    {
        Debug.Log("In Triggerstay" + col.name);
        HaloTrafficLight halo = col.gameObject.GetComponent<HaloTrafficLight>();
        Debug.Log("wats the light " + halo.greenLightHalo.activeSelf);
        if (halo.greenLightHalo.activeSelf)
        {
            Debug.Log("Inside the condition");
            navagent.speed = 4f;

        }
    }


    void OnTriggerEnter(Collider collider)
    {

        Debug.Log("On trigger enter " + collider.name);
        HaloTrafficLight halo = collider.gameObject.GetComponent<HaloTrafficLight>();
        Debug.Log("Is green light" + halo.greenLightHalo.activeSelf);
        Debug.Log("Is red light" + halo.redLightHalo.activeSelf);
        Debug.Log("Is yellow light" + halo.yellowLightHalo.activeSelf);
        if (collider.tag.Equals("TrafficLightCollider") && halo.greenLightHalo.activeSelf)
        {
            navagent.speed = 3.5f;
        }
        else
        {
            Debug.Log("am i in not green" + halo.greenLightHalo.activeSelf);
            navagent.speed = 0.001f;
        }
    }
}