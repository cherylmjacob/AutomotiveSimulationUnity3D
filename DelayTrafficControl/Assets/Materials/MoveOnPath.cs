﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveOnPath : MonoBehaviour {

    public Path pathtofollow;

    public int currentwaypointid=0;

    public float speed;

    private float reachDistance = 1.0f;

    public float rotationspeed = 5.0f;

    public string pathname;

    public HaloTrafficLight trafficLightState;

    

    Vector3 last_pos;
    Vector3 cur_pos;

	// Use this for initialization
	void Start () {

        //pathtofollow = GameObject.Find(pathname).GetComponent<Path>();

        last_pos = transform.position;
	}

    // Update is called once per frame
    void Update()
    {
        if (currentwaypointid < 2)
        {
            float distance = Vector3.Distance(pathtofollow.path_objs[currentwaypointid].position, transform.position);
            transform.position = Vector3.MoveTowards(transform.position, pathtofollow.path_objs[currentwaypointid].position, speed);
            var rotation = Quaternion.LookRotation(pathtofollow.path_objs[currentwaypointid].position - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotationspeed);

            if (distance <= reachDistance)
            {
                currentwaypointid++;

            }

            if (currentwaypointid >= pathtofollow.path_objs.Count)
            {
                //currentwaypointid = 0;
            }

        }
    }

    void OnTriggerStay(Collider col)
    {
       Debug.Log("In Triggerstay" +col.name);
        HaloTrafficLight halo = col.gameObject.GetComponent<HaloTrafficLight>();
        Debug.Log("wats the light " + halo.greenLightHalo.activeSelf);
        if (halo.greenLightHalo.activeSelf)
        {
            Debug.Log("Inside the condition" );
            speed = 0.1f;
        }
    }


    void OnTriggerEnter(Collider collider)
    {

        Debug.Log("On trigger enter " + collider.name);
        HaloTrafficLight halo = collider.gameObject.GetComponent<HaloTrafficLight>();
        Debug.Log("Is green light" + halo.greenLightHalo.activeSelf);
        Debug.Log("Is red light" + halo.redLightHalo.activeSelf);
        Debug.Log("Is yellow light" + halo.yellowLightHalo.activeSelf);
        if (collider.tag.Equals("TrafficLightCollider") && halo.greenLightHalo.activeSelf) 
        {
            speed = 0.05f;
        }
        else
        {
            Debug.Log("am i in not green" + halo.greenLightHalo.activeSelf);
            speed = 0.001f;
        }
    }
}
