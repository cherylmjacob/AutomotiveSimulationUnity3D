﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class manscript : MonoBehaviour {


    public Path pathtofollow;

    public int currentwaypointid = 0;

    public float speed;

    private float reachDistance = 1.0f;

    public float rotationspeed = 5.0f;

    public string pathname;

    public HaloTrafficLight trafficLightState;

    public GameObject[] respawns;

    Vector3 last_pos;
    Vector3 cur_pos;
    float posx, posy, x, z;
    // Use this for initialization
    void Start()
    {

        //pathtofollow = GameObject.Find(pathname).GetComponent<Path>();

        last_pos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (currentwaypointid < 2)
        {
            float distance = Vector3.Distance(pathtofollow.path_objs[currentwaypointid].position, transform.position);
            transform.position = Vector3.MoveTowards(transform.position, pathtofollow.path_objs[currentwaypointid].position, speed);
            var rotation = Quaternion.LookRotation(pathtofollow.path_objs[currentwaypointid].position - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotationspeed);

            if (distance <= reachDistance)
            {
                currentwaypointid++;

            }

            if (currentwaypointid >= pathtofollow.path_objs.Count)
            {
                //currentwaypointid = 0;
            }

        }
    }
}
