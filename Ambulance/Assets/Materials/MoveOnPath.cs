﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MoveOnPath : MonoBehaviour
{


    public Path pathtofollow;

    public HaloTrafficLight trafficLightState;
    public int i = 0;

    public Camera cam;
    public NavMeshAgent navagent1;
    public NavMeshPath navpath;

    float x1, x2, z1, z2, x3, x4, z3, z4;

    GameObject dest;
    // Use this for initialization
    void Start()
    {
        cam = Camera.main;
        navagent1 = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {

        if (i == 0)
        {
            navagent1.SetDestination(pathtofollow.path_objs[0].position);

        }
        Debug.Log("Path pos1" + pathtofollow.path_objs[0].position);
        Debug.Log("obj pos1" + transform.position);
        x1 = transform.position.x;
        x2 = pathtofollow.path_objs[0].position.x;
        z1 = transform.position.z;
        z2 = pathtofollow.path_objs[0].position.z;



        if (x1 == x2 && z1 == z2)
        {
            i = 1;
        }

        if (i == 1)
        {
            navagent1.SetDestination(pathtofollow.path_objs[1].position);

        }
        x3 = navagent1.transform.position.x;
        x4 = pathtofollow.path_objs[1].position.x;
        z3 = navagent1.transform.position.z;
        z4 = pathtofollow.path_objs[1].position.z;

        Debug.Log("newx" + pathtofollow.path_objs[1].position.x);
        Debug.Log("new1x" + navagent1.transform.position.x);
        Debug.Log("newz" + pathtofollow.path_objs[1].position.z);
        Debug.Log("new1z" + navagent1.transform.position.z);
        if (x3 == x4 && z3 == z4)
        {
            i = 2;
        }

        if (i == 2)
        {
            navagent1.SetDestination(pathtofollow.path_objs[2].position);
            //            navagent1.speed = 0.0001f;
            //#if UNITY_EDITOR
            //            UnityEditor.EditorApplication.isPlaying = false;
            //#elif UNITY_WEBPLAYER
            //                         Application.OpenURL(webplayerQuitURL);
            //#else
            //                         Application.Quit();
            //#endif
        }

    }

    void OnTriggerStay(Collider col)
    {
        if (col.tag.Equals("Player"))
        {
            navagent1.speed = 0.000001f;
        }
        if (col.tag.Equals("TrafficLightCollider"))
        {
            Debug.Log("In Triggerstay" + col.name);
            HaloTrafficLight halo = col.gameObject.GetComponent<HaloTrafficLight>();
            Debug.Log("wats the light " + halo.greenLightHalo.activeSelf);
            if (halo.greenLightHalo.activeSelf)
            {
                Debug.Log("Inside the condition");
                navagent1.speed = 4f;

            }
        }
    }


    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag.Equals("AIThirdPersonController"))
        {
            navagent1.speed = 0.000001f;
        }
        if (collider.tag.Equals("TrafficLightCollider"))
        {

            Debug.Log("On trigger enter " + collider.name);
            HaloTrafficLight halo = collider.gameObject.GetComponent<HaloTrafficLight>();
            Debug.Log("Is green light" + halo.greenLightHalo.activeSelf);
            Debug.Log("Is red light" + halo.redLightHalo.activeSelf);
            Debug.Log("Is yellow light" + halo.yellowLightHalo.activeSelf);
            if (collider.tag.Equals("TrafficLightCollider") && halo.greenLightHalo.activeSelf)
            {
                navagent1.speed = 3.5f;
            }
            else
            {
                Debug.Log("am i in not green" + halo.greenLightHalo.activeSelf);
                navagent1.speed = 0.001f;

			}
        }
    }
    private void OnTriggerExit(Collider collid)
    {
        navagent1.speed = 5.0f;
    }
}