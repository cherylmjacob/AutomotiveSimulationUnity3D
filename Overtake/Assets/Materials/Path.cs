﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Path : MonoBehaviour {

public Color raycolor=Color.white;
   public List<Transform> path_objs= new List<Transform>();
    Transform[] theArray;

  

    void OnDrawGizmosSelected()
    {
        Gizmos.color = raycolor;
        theArray= GetComponentsInChildren<Transform>();

        path_objs.Clear();

       // node = new List<Transform>();

        foreach(Transform path_obj in theArray)
        {
            if (path_obj != this.transform)
            {
                path_objs.Add(path_obj);
            }

            
        }

        for (int i = 0; i < path_objs.Count; i++)
        {
            Vector3 currentnode = path_objs[i].position;
            Vector3 previousnode= Vector3.zero;
            if (i > 0)
            {
                previousnode = path_objs[i - 1].position;
                Gizmos.DrawLine(previousnode, currentnode);
                Gizmos.DrawWireSphere(currentnode, 0.3f);
            }
            //else if (i == 0 && path_objs.Count > 1)
            //{
            //    previousnode = path_objs[path_objs.Count - 1].position;

            //}
            //Gizmos.DrawLine(previousnode, currentnode);
            //Gizmos.DrawWireSphere(currentnode, 0.8f);
        }

    }
}
